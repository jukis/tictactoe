var mongoose = require('mongoose');
var Schema = mongoose.Schema;

userSchema = new Schema
({
    username: {type: String, required: true},
    email: {type: String, required: true, unique: true},
    password: {type: String, required: true},
})
user = mongoose.model('user',userSchema);

module.exports = user;