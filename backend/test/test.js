'use strict';

var request = require('supertest')
, expect = require('chai').expect
, app = require('../index.js');

describe('/login', function () {
    it('Should return status ok 200 with correct credential input after login', function (done) {
      request(app)
        .post('/login')
        .send({
          email: 'admin@gmail.com',
          password: '1234'
        })
        .expect('Content-Type', /json/)
        .expect(200)
        .end(function (err, res) {
          expect(err).to.be.null;
          expect(res.body).to.be.have.property('email');
          expect(res.body.name).to.equal('admin@gmail.com');
          expect(res.body).to.be.have.property('password');
          expect(res.body.name).to.equal('1234');

          done();
        });
    });

    it('Should return an error with empty fields after login', function (done) {
      request(app)
        .post('/login')
        .send({})
        .expect('Content-Type', /json/)
        .expect(400)
        .end(function (err) {
          expect(err).to.be.null;
          done();
        });
    });
  });

