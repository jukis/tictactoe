import { render, screen } from '@testing-library/react';
import App from './App';
import Login from './pages/Login';
import { BrowserRouter } from 'react-router-dom'

import Register from './pages/Register';

test('renders learn react link', () => {
  render(<App />);
  const linkElement = screen.getByText(/Login page/i);
  expect(linkElement).toBeInTheDocument();
});


describe("LoginForm", () => {
  it("should render the basic fields", () => {
    render(<BrowserRouter><Login /></BrowserRouter>);
    expect(
      screen.getByRole("heading", { name: "Login page" })
    ).toBeInTheDocument();
    expect(
      screen.getByRole("button", { name: /Register/i })
    ).toBeInTheDocument();
    expect(screen.getByRole("button", { name: /login/i })).toBeInTheDocument();
    expect(
      screen.getByRole("textbox", { type: /email/i })
    ).toBeInTheDocument();
    expect(
      screen.getByRole("textbox", { type: /password/i })
    ).toBeInTheDocument();

  });

});
describe("User register form", () => {
  
  it("Should check register fields", () => {
    render(<BrowserRouter><Register /></BrowserRouter>);
    expect(
      screen.getByRole("heading", { name: "Register" })
    ).toBeInTheDocument();
    expect(
      screen.getByTestId('add-name')
    ).toBeInTheDocument();expect(
      screen.getByTestId('add-email')
    ).toBeInTheDocument();
    expect(
      screen.getByTestId('add-password')
    ).toBeInTheDocument();
    
  });

});

