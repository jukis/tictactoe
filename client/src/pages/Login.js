import '../App.css';
import { useNavigate } from "react-router-dom";
import React, { useState } from 'react';

function Login(props) {
  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')

  async function loginUser(event) {
    event.preventDefault()
    
    const response = await fetch('http://localhost:1337/api/login', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        email,
        password,
      }),
    })

    const data = await response.json()

    if (data.user) {
      props.setUser(data.user)
      window.location.href = '/'
    } else {
      alert('Please check your username and password')
    }
  }
  const navigate = useNavigate();

  function handleClick() {
    navigate("/register");
  }

  return (

    <div>
      <h1>Login page</h1>
      <form onSubmit={loginUser}>
        <input
          value={email}
          onChange={(e) => setEmail(e.target.value)}
          type="email"
          placeholder="Email"
        />
        <br />
        <input
          value={password}
          onChange={(e) => setPassword(e.target.value)}
          type="password"
          placeholder="Password"
        />
        <br />
        <input type="submit" value="Login" />
        <button type="button" onClick={handleClick}>
          Register</button>
      </form>
    </div>




  );
}

export default Login;
