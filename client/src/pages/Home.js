import { Navigate } from 'react-router-dom';

import '../App.css';

export const Home = (props) => {
  if (!props.user)
    return <Navigate to="login" replace />;

  return (
    <div className="Home">
      <h1>Home page, Hello {props.user}</h1>
    </div>
  );
}

export default Home;
